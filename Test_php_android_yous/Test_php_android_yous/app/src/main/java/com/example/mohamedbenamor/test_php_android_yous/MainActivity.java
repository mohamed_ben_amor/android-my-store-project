package com.example.mohamedbenamor.test_php_android_yous;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    public static String ipAddress;
    public static String port;
    public static String url;
    public static String url1;
    ListCatModel listCatModel;
    List<Categories> cats= new ArrayList<Categories>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        ipAddress = preferences.getString("ip_address","10.2.120.5");
        port = preferences.getString("port","8088");

        url= "http://"+ipAddress+":"+port;
        url1= "http://"+ipAddress+":"+port+"/cat/categories.php";


        listCatModel = new ListCatModel(this,R.layout.catitem,cats);
        ListView listView = (ListView) findViewById(R.id.listViewCat);
        listView.setAdapter(listCatModel);

        new MyTask().execute(url1);

       // Toast.makeText(getApplicationContext(), url, Toast.LENGTH_LONG).show();

    }

    public StringBuffer getHttpResponse(String url)
    {
        StringBuffer stringBuffer = new StringBuffer();

        try {
                HttpClient client = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse response = client.execute(httpGet);
            if(response.getStatusLine().getStatusCode()==200)
            {
                HttpEntity httpEntity = response.getEntity();
                InputStream is=httpEntity.getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String str;
                while((str=br.readLine())!=null)
                {
                    stringBuffer.append(str);
                }
            }
        } catch (Exception e) {


            e.printStackTrace();
        }

        return stringBuffer;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.options:
                Intent intent_option = new Intent(this,SettingActivity.class);
                startActivity(intent_option);
                return true;
            default: break;
        }

        return super.onOptionsItemSelected(item);
    }


    class MyTask extends AsyncTask<String,Integer,StringBuffer>{

        @Override
        protected StringBuffer doInBackground(String... args) {
            String url =args[0];
            StringBuffer stringBuf=getHttpResponse(url);
            return stringBuf;
        }

        //s'execute après doInBackground
        @Override
        protected void onPostExecute(StringBuffer result) {
            Gson gson= new Gson();
            //Toast.makeText(getApplicationContext(),"dsds"+result.toString(),Toast.LENGTH_LONG).show();
            Categories[] categories=gson.fromJson(result.toString(),Categories[].class);
            for(Categories c:categories)
                    cats.add(c);
            listCatModel.notifyDataSetChanged();
            super.onPostExecute(result);
        }
    }
}


