package com.example.mohamedbenamor.test_php_android_yous;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Created by Mohamed.BenAmor on 07/12/2015.
 */
public class ListCatModel extends ArrayAdapter<Categories> {

    private List<Categories> categories;
    public ListCatModel(Context context, int resource, List<Categories> cats) {
        super(context, resource,cats);
        this.categories=cats;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.catitem,parent,false);
        TextView textv = (TextView) rowView.findViewById(R.id.textView);
        final ImageView imgV = (ImageView) rowView.findViewById(R.id.imageViewCat);

        textv.setText(categories.get(position).getTitle());

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String imagename = categories.get(position).getPhoto();
                    InputStream is = new URL(MainActivity.url+"/cat/images/"+imagename).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                    imgV.setImageBitmap(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();



        return rowView;
    }
}
