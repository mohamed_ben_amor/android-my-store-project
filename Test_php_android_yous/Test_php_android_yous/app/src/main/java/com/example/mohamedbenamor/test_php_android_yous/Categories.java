package com.example.mohamedbenamor.test_php_android_yous;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mohamed.BenAmor on 07/12/2015.
 */
public class Categories implements Serializable {

    @SerializedName("categoryID")
    private Long id_cat;
    @SerializedName("categoryName")
    private String title;
    @SerializedName("categoryDescription")
    private String description;
    @SerializedName("photo")
    private String photo;

    public Categories(Long id_cat, String title, String description, String photo) {
        this.id_cat = id_cat;
        this.title = title;
        this.description = description;
        this.photo = photo;
    }

    public void setId_cat(Long id_cat) {
        this.id_cat = id_cat;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getId_cat() {
        return id_cat;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoto() {
        return photo;
    }
}
