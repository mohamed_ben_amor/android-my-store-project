package org.kiwi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="publicite")
public class Publicite {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idPublicite;
	private String photo;
	public Long getIdPublicite() {
		return idPublicite;
	}
	public void setIdPublicite(Long idPublicite) {
		this.idPublicite = idPublicite;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Publicite(String photo) {
		super();
		this.photo = photo;
	}
	
	
	public Publicite() {
		super();
	
	}

}
