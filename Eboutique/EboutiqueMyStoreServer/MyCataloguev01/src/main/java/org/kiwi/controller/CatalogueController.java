package org.kiwi.controller;

import java.util.List;

import org.kiwi.dao.IProduitRepository;
import org.kiwi.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;




@RestController
public class CatalogueController {
    
	@Autowired
	private IProduitRepository produitRepository;
	
		
		
	
	
	@RequestMapping("/test")
	public String test()
	{
		return "test";
	}
	
	@RequestMapping("/save")
	public Produit saveProduit(Produit p)
	{
		produitRepository.save(p);
		return p;
	}
	
	@RequestMapping("/all")
	public List<Produit> getProduits()
	{
		
		return produitRepository.findAll();
	}
	
	@RequestMapping("/produits")
	public Page<Produit> getProduits(int page)
	{
		return produitRepository.findAll(new PageRequest(page, 5));
		
	}
	
	@RequestMapping("/produitsParMC")
	public Page<Produit> getProduits(String mc,int page)
	{
		return produitRepository.produitParMC("%"+mc+"%", new PageRequest(page, 5));
	}
	
	@RequestMapping("/get")
	public Produit getProduits(Long ref)
	{
		return produitRepository.findOne(ref);
	}
	
	
	
	@RequestMapping("/delete")
	public Boolean deleteProduits(Long ref)
	{
		produitRepository.delete(ref);
		return true;
	}
	
	@RequestMapping("/update")
	public Produit update(Produit p)
	{
		produitRepository.saveAndFlush(p);
		return p;
	}
	
	
	
}
