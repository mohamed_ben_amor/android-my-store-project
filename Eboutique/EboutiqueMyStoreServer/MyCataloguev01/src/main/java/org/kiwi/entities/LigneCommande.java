package org.kiwi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ForeignKey;
@Entity	
public class LigneCommande {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name="idProduit")
	private Produit produit;
	private int quantite;
	private Double prix;
	@ManyToOne
	@JoinColumn(name="idCommande")
	private Commande Commande;
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Produit getProduit() {
		return produit;
	}
	public void setProduit(Produit produit) {
		this.produit = produit;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public Commande getCommande() {
		return Commande;
	}
	public void setCommande(Commande commande) {
		Commande = commande;
	}
	public LigneCommande(Produit produit, int quantite, double prix) {
		super();
		this.produit = produit;
		this.quantite = quantite;
		this.prix = prix;
	}
	public LigneCommande() {
		super();
	}

	
	

}
