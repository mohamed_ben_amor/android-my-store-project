package org.kiwi.controller;

import java.util.List;

import org.kiwi.dao.IPubliciteRepository;
import org.kiwi.entities.Categorie;
import org.kiwi.entities.Publicite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PubliciteController {
	
	@Autowired
	private IPubliciteRepository publiciteRepository;
	
	@RequestMapping("/allPublicite")
	public List<Publicite> getProduits()
	{
		
		return publiciteRepository.findAll();
	}
	
	
	@RequestMapping(value = "/pub",method = RequestMethod.POST)
	public ResponseEntity<Publicite> postUpdatepublicite(@RequestBody Publicite pub)
	{
		if(pub != null)
		{
			pub.setPhoto("10.jpg");
			publiciteRepository.saveAndFlush(pub);
		}
		return new ResponseEntity<Publicite>(pub,HttpStatus.OK);
	}
	
    @RequestMapping(value = "/addPublicite", 
            method = RequestMethod.POST,
            headers = {"Content-type=application/json"})
    @ResponseBody
    public String addPub(@RequestBody Publicite p) {
    	publiciteRepository.save(p);
    return "Saved categorie: " + p.toString();
}
	
	

}
