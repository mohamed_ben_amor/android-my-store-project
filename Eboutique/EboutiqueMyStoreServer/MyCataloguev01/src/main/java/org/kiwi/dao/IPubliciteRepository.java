package org.kiwi.dao;

import org.kiwi.entities.Publicite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPubliciteRepository extends JpaRepository<Publicite, Long>{

	
}
