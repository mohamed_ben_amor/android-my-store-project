package org.kiwi.dao;

import org.kiwi.entities.Categorie;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.domain.*;
import java.util.List;

//la couche DAO
public interface ICategorieRepository extends JpaRepository<Categorie, Long> {
	
	@Query("select c from Categorie c where c.description like :x")
	public Page<Categorie> categorieParMC(@Param("x")String mc,Pageable p);
	
	public List<Categorie> findBynomCategorie(String des);
	public Page<Categorie> findBynomCategorie(String des,Pageable p);
	
}
