package org.kiwi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCataloguev01Application {

    public static void main(String[] args) {
        SpringApplication.run(MyCataloguev01Application.class, args);
    }
}
