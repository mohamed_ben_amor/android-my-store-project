package org.kiwi.controller;

import java.util.List;

import org.kiwi.dao.ICategorieRepository;
import org.kiwi.entities.Categorie;
import org.kiwi.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class CategoriesController {
    
	/*INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("accessoir desc ","Accessoir","accessoir.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Composant desc ","Composant","Composants.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Imprimante desc ","Imprimante","imprimante.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Ordinateur desc ","Ordinateur","ordinateur.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Reseau desc ","Reseau","reseau.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Smartphone desc ","Smartphone","Smartphone.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Tablette desc ","Tablette","tablette.jpg");
	INSERT INTO `categorie`(`description`, `nom_categorie`, `nom_photo`) VALUES ("Televisuer desc ","Televisuer","televisuer.jpg");*/
	
	/*INSERT INTO `publicite`(`photo`) VALUES ("01.jpg");
	INSERT INTO `publicite`(`photo`) VALUES ("02.jpg");
	INSERT INTO `publicite`(`photo`) VALUES ("03.jpg");*/
	
	
	@Autowired
	private ICategorieRepository categorieRepository;
	
	
	@RequestMapping(value = "/",method = RequestMethod.POST)
	public ResponseEntity<Categorie> postUpdateProduit(@RequestBody Categorie categorie)
	{
		if(categorie != null)
		{
			categorie.setDescription("test update description");
			categorieRepository.saveAndFlush(categorie);
		}
		return new ResponseEntity<Categorie>(categorie,HttpStatus.OK);
	}
	
@RequestMapping(value = "/addCategorie", 
            method = RequestMethod.POST,
            headers = {"Content-type=application/json"})
@ResponseBody
public String addPerson(@RequestBody Categorie c) {
	categorieRepository.save(c);
return "Saved categorie: " + c.toString();
}
	
	
	@RequestMapping("/testCategorie")
	public String test()
	{
		return "test";
	}
	
	@RequestMapping("/saveCategorie")
	public Categorie saveCategorie(Categorie p)
	{
		categorieRepository.save(p);
		return p;
	}
	
	
	@RequestMapping("/allCategorie")
	public List<Categorie> getProduits()
	{
		
		return categorieRepository.findAll();
	}
	
	@RequestMapping("/Categories")
	public Page<Categorie> getProduits(int page)
	{
		return categorieRepository.findAll(new PageRequest(page, 5));
		
	}
	
	@RequestMapping("/CategoriesParMC")
	public Page<Categorie> getCategorie(String mc,int page)
	{
		return categorieRepository.categorieParMC("%"+mc+"%", new PageRequest(page, 5));
	}
	
	@RequestMapping("/getCategorie")
	public Categorie getProduits(Long ref)
	{
		return categorieRepository.findOne(ref);
	}
	
	
	
	@RequestMapping("/deleteCategorie")
	public Boolean deleteProduits(Long ref)
	{
		categorieRepository.delete(ref);
		return true;
	}
	
	@RequestMapping("/updateCategorie")
	public Categorie update(Categorie c)
	{
		categorieRepository.saveAndFlush(c);
		return c;
	}
	
	
	
}
