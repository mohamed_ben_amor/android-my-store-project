package org.kiwi.dao;

import org.kiwi.entities.Produit;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.data.domain.*;
import java.util.List;

//la couche DAO
public interface IProduitRepository extends JpaRepository<Produit, Long> {
	
	@Query("select p from Produit p where p.designation like :x")
	public Page<Produit> produitParMC(@Param("x")String mc,Pageable p);
	
	public List<Produit> findByDesignation(String des);
	public Page<Produit> findByDesignation(String des,Pageable p);
	

}
