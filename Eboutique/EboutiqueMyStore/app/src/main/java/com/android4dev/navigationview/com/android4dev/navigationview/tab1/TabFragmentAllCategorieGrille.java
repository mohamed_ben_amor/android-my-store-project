package com.android4dev.navigationview.com.android4dev.navigationview.tab1;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android4dev.navigationview.MainActivity;
import com.android4dev.navigationview.R;
import com.android4dev.navigationview.com.android4dev.navigationview.adapter.GridViewCategoriesCustomAdapter;
import com.android4dev.navigationview.com.android4dev.navigationview.adapter.GridViewCategoriesCustomAdapterDemo;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Categorie;
import com.android4dev.navigationview.com.android4dev.navigationview.utility.WebCommunication;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohamed.BenAmor on 15/12/2015.
 */
public class TabFragmentAllCategorieGrille extends Fragment implements AdapterView.OnItemClickListener {


    Communicator com;
    GridViewCategoriesCustomAdapter listGridModel;
    GridViewCategoriesCustomAdapterDemo listGridModelDemo;
    List<Categorie> listCat= new ArrayList<Categorie>();
    GridView gridView;
    Boolean onLineMode=false;

    public void setCommunicator(Communicator comm)
    {
         com = comm;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      //  Toast.makeText(getContext(),"oncreateview",Toast.LENGTH_LONG).show();
        View view = inflater.inflate(R.layout.categorie_produit_grille,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        //Toast.makeText(getContext(),"onactivitycreated",Toast.LENGTH_LONG).show();
        super.onActivityCreated(savedInstanceState);
        listGridModel = new GridViewCategoriesCustomAdapter(getActivity(),R.layout.categorie_produit_grille_row,listCat);
        listGridModelDemo = new GridViewCategoriesCustomAdapterDemo(getActivity(),R.layout.categorie_produit_grille_row,listCat);
        gridView = (GridView) getActivity().findViewById(R.id.gridViewCustom);


        if(onLineMode == true) {

            if (listGridModel == null || gridView == null) {
                Toast.makeText(getActivity(), "a null object detected !!!", Toast.LENGTH_LONG).show();
            } else {

                new MyTask().execute(MainActivity.url);

            }

            gridView.setAdapter(listGridModel);
            gridView.refreshDrawableState();
            super.onActivityCreated(savedInstanceState);
        }
        else if(onLineMode == false)
        {

            new MyTaskOffLine().execute();

            gridView.setAdapter(listGridModelDemo);
            gridView.refreshDrawableState();
            super.onActivityCreated(savedInstanceState);


        }

    }
/*

    @Override
    public void onStart() {
      //  Toast.makeText(getContext(),"onStart",Toast.LENGTH_LONG).show();
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        Toast.makeText(getContext(),"onattach",Toast.LENGTH_LONG).show();
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        Toast.makeText(getContext(),"onresume",Toast.LENGTH_LONG).show();

        super.onResume();
    }


    @Override
    public void onDestroy() {

        Toast.makeText(getContext(),"ondestroy",Toast.LENGTH_LONG).show();
        super.onDestroy();
    }
*/
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        com.response(position);
    }


    interface Communicator{

       void response(int index);

    }

    class MyTask extends AsyncTask<String,Integer,StringBuffer> {

        @Override
        protected StringBuffer doInBackground(String... args) {
            String url =args[0];
            StringBuffer stringBuf= WebCommunication.getHttpResponse(url);



            return stringBuf;
        }

        //s'execute après doInBackground
        @Override
        protected void onPostExecute(StringBuffer result) {
            Gson gson= new Gson();
            //Toast.makeText(getApplicationContext(),"dsds"+result.toString(),Toast.LENGTH_LONG).show();

            Categorie[] categories=gson.fromJson(result.toString(),Categorie[].class);
            if(categories !=null) {
                for (Categorie c : categories)
                    listCat.add(c);
                listGridModel.notifyDataSetChanged();
            }
            else
            {
                Toast.makeText(getActivity(),"cannot download data from server !!!!"+result.toString(),Toast.LENGTH_LONG).show();
            }
            super.onPostExecute(result);
        }
    }

    class MyTaskOffLine extends AsyncTask<String,Integer,List<Categorie>> {

        @Override
        protected List<Categorie> doInBackground(String... args) {
            List<Categorie> localListCat= new ArrayList<Categorie>();

            Categorie c1 = new Categorie("Accessoir","accessoir");
            Categorie c2 = new Categorie("Ordinateur","ordinateur");
            Categorie c3 = new Categorie("Imprimante","imprimante");
            Categorie c4 = new Categorie("Reseau","reseau");
            Categorie c5 = new Categorie("Téléviseur","televisuer");
            Categorie c6 = new Categorie("Tablette","tablette");
            Categorie c7 = new Categorie("Smartphone","smartphone");
            Categorie c8 = new Categorie("Composants","composants");

            localListCat.add(c1);
            localListCat.add(c2);
            localListCat.add(c3);
            localListCat.add(c4);
            localListCat.add(c5);
            localListCat.add(c6);
            localListCat.add(c7);
            localListCat.add(c8);

            return localListCat;
        }

        //s'execute après doInBackground
        @Override
        protected void onPostExecute(List<Categorie>  result) {

            if(result !=null) {
                for (Categorie c : result)
                    listCat.add(c);
                listGridModel.notifyDataSetChanged();
            }
            else
            {
                Toast.makeText(getActivity(),"cannot download data from server !!!!"+result.toString(),Toast.LENGTH_LONG).show();
            }
            super.onPostExecute(result);
        }
    }




}
