package com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Administrateur on 03/01/2016.
 */
public class Publicite implements Serializable {

    @SerializedName("idPublicite")
    private Long idPublicite;

    @SerializedName("photo")
    private String photo;

    public Publicite() {
        super();
    }

    public Publicite(Long idPublicite, String photo) {
        this.idPublicite = idPublicite;
        this.photo = photo;
    }

    public Long getIdPublicite() {
        return idPublicite;
    }

    public String getPhoto() {
        return photo;
    }

    public void setIdPublicite(Long idPublicite) {
        this.idPublicite = idPublicite;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
