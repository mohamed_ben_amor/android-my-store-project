package com.android4dev.navigationview;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Admin on 04-06-2015.
 * http://www.androidhive.info/2013/08/android-working-with-google-maps-v2/
 */
public class ContentTrouverMagasin extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private final String titleActionBar = "Trouver Magasin";
    private GoogleMap googleMap;

    double latitude1 = 36.816448;
    double longitude1 = 10.179061;

    double latitude2 = 36.783423;
    double longitude2 = 9.642123;

    double latitude3 = 36.180544;
    double longitude3 = 9.578952;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_trouver_magasin);

        // Initializing Drawer Layout and ActionBarToggle
        toolbar = (Toolbar) findViewById(R.id.toolbar_trouverMag);
        setSupportActionBar(toolbar);
        loadMap();



    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }


    private void loadMap()
    {
        try {
            // Loading map
            initilizeMap();

            MarkerOptions marker1 = new MarkerOptions().position(new LatLng(latitude1, longitude1)).title("Magasin 1");
            marker1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            marker1.snippet(" Adresse magasin 1");
            marker1.draggable(true);


            MarkerOptions marker2 = new MarkerOptions().position(new LatLng(latitude2, longitude2)).title("Magasin 2");
            marker2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            marker2.snippet(" Adresse magasin 2");
            marker2.draggable(true);

            MarkerOptions marker3 = new MarkerOptions().position(new LatLng(latitude3, longitude3)).title("Magasin 3");
            marker3.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            marker3.snippet(" Adresse magasin 3");
            marker3.draggable(true);


            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude1, longitude1)).zoom(7).build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            googleMap.addMarker(marker1);
            googleMap.addMarker(marker2);
            googleMap.addMarker(marker3);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_panier_trouvMag:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        getMenuInflater().inflate(R.menu.menu_trouvermag, menu);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(titleActionBar);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setHomeButtonEnabled(true);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();

    }
}
