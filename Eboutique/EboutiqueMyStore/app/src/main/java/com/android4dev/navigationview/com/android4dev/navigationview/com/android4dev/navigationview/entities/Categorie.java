package com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mohamed.BenAmor on 14/12/2015.
 */
public class Categorie implements Serializable {


    @SerializedName("idcategorie")
    private Long idcategorie;
    @SerializedName("nomCategorie")
    private String nomCategorie;
    @SerializedName("description")
    private String description;
    @SerializedName("photo")
    private byte[] photo;
    @SerializedName("nomPhoto")
    private String nomPhoto;


    public Categorie(Long idcategorie, String nomCategorie, String description, byte[] photo, String nomPhoto) {
        this.idcategorie = idcategorie;
        this.nomCategorie = nomCategorie;
        this.description = description;
        this.photo = photo;
        this.nomPhoto = nomPhoto;
    }


    public Categorie(String nomCategorie) {

        this.nomCategorie = nomCategorie;

    }

    public Categorie(String nomCategorie,String nomPhoto) {

        this.nomCategorie = nomCategorie;
        this.nomPhoto = nomPhoto;

    }





    public void setIdcategorie(Long idcategorie) {
        this.idcategorie = idcategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public void setNomPhoto(String nomPhoto) {
        this.nomPhoto = nomPhoto;
    }

    public Long getIdcategorie() {
        return idcategorie;
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getNomPhoto() {
        return nomPhoto;
    }
}
