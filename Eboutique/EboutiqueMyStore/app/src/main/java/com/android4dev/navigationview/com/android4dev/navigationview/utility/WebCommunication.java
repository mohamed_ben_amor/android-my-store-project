package com.android4dev.navigationview.com.android4dev.navigationview.utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Mohamed.BenAmor on 15/12/2015.
 */
public class WebCommunication {


    public static StringBuffer getHttpResponse(String url)
    {
        StringBuffer stringBuffer = new StringBuffer();

        try {
            URL urlObj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection)urlObj.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();

            InputStream inputStream = connection.getInputStream();

            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);

            }
        } catch (Exception e) {


            e.printStackTrace();
        }

        return stringBuffer;
    }



}
