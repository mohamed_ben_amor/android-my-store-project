package com.android4dev.navigationview.com.android4dev.navigationview.adapter;

/**
 * Created by Mohamed.BenAmor on 14/12/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android4dev.navigationview.R;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Categorie;

import java.util.List;


public class ListCatModelDemo extends ArrayAdapter<Categorie> {

    private List<Categorie> categories;
    public ListCatModelDemo(Context context, int resource, List<Categorie> cats) {
        super(context, resource,cats);
        this.categories=cats;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.categorie_item,parent,false);
        TextView textv = (TextView) rowView.findViewById(R.id.txt_categorie_title);
        final ImageView imgV = (ImageView) rowView.findViewById(R.id.img_categorie);

        textv.setText(categories.get(position).getNomCategorie());
        imgV.setImageResource(getImageId(getContext(),categories.get(position).getNomPhoto()));
        return rowView;
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }


}
