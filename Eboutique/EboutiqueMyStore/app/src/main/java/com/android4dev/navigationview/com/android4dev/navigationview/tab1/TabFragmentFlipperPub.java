package com.android4dev.navigationview.com.android4dev.navigationview.tab1;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android4dev.navigationview.MainActivity;
import com.android4dev.navigationview.R;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Categorie;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Publicite;
import com.android4dev.navigationview.com.android4dev.navigationview.utility.WebCommunication;
import com.google.gson.Gson;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrateur on 02/01/2016.
 */
public class TabFragmentFlipperPub extends Fragment {

    private Animation.AnimationListener mAnimationListener;
    ViewFlipper vf;

    List<Publicite> listpub= new ArrayList<Publicite>();
    @Override
    public void onResume() {
        super.onResume();
        int gallery_grid_Images[]={R.drawable.color_baloons,R.drawable.lightning,R.drawable.lightning};
        //int gallery_grid_Images[]={R.drawable.color_baloons,R.drawable.lightning};
        String url = "http://192.168.100.3:8888/allPublicite";


        new MyTaskPublicite().execute(url);
Toast.makeText(getActivity().getApplicationContext()," "+listpub.size(),Toast.LENGTH_LONG).show();


      /*  for(int i=0;i<gallery_grid_Images.length;i++)
        {
            //  This will create dynamic image view and add them to ViewFlipper
            setFlipperImage(gallery_grid_Images[i],vf);
        }*/



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pub_flipper,container,false);
        vf = (ViewFlipper)view.findViewById(R.id.flipperPub);

            //ImageView imgv = new ImageView(getActivity());
            //ImageView imgv1 = new ImageView(getActivity());
            //imgv.setImageResource(R.drawable.color_baloons);
            //imgv1.setImageResource(R.drawable.color_baloons);
            //imgv.setLayoutParams(new FrameLayout.LayoutParams(120, 30, Gravity.CENTER));
            //imgv1.setImageResource(R.drawable.lightning);
            //vf.addView(imgv,0);
            //vf.addView(imgv1,0);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {



        super.onActivityCreated(savedInstanceState);
    }

    private void setFlipperImage(int res,ViewFlipper viewFlippe) {
        //Log.i("Set Filpper Called", res + "");
        ImageView image = new ImageView(getActivity().getApplicationContext());
        image.setBackgroundResource(res);
       // image.setImageBitmap(res);
        viewFlippe.addView(image);
    }

    class MyTaskPublicite extends AsyncTask<String,Integer,StringBuffer> {

        @Override
        protected StringBuffer doInBackground(String... args) {
            String url =args[0];
            StringBuffer stringBuf= WebCommunication.getHttpResponse(url);
            return stringBuf;
        }

        //s'execute après doInBackground
        @Override
        protected void onPostExecute(StringBuffer result) {
            Gson gson= new Gson();

            Publicite[] publicites=gson.fromJson(result.toString(),Publicite[].class);
            if(publicites !=null) {
                //Toast.makeText(getActivity().getApplicationContext(),"dsds"+result.toString(),Toast.LENGTH_LONG).show();
                for (Publicite c : publicites) {
                    // listpub.add(c);


                    String s = MainActivity.urlIndex + "/pub/" + c.getPhoto();
                    Toast.makeText(getActivity().getApplicationContext(),"00",Toast.LENGTH_LONG).show();
                    final ImageView image = new ImageView(getActivity().getApplicationContext());
                    try {
                        String imagename = c.getPhoto();
                        Toast.makeText(getActivity().getApplicationContext(),"0",Toast.LENGTH_LONG).show();
                        InputStream is = new URL(MainActivity.urlIndex + "/pub/" + imagename).openStream();
                        Toast.makeText(getActivity().getApplicationContext(),"1",Toast.LENGTH_LONG).show();
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        Toast.makeText(getActivity().getApplicationContext(),"2",Toast.LENGTH_LONG).show();
                        image.setImageBitmap(bitmap);
                        Toast.makeText(getActivity().getApplicationContext(),"3",Toast.LENGTH_LONG).show();
                        vf.addView(image);
                        Toast.makeText(getActivity().getApplicationContext(),"4",Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                vf.setInAnimation(AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.left_in));
                vf.setOutAnimation(AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.left_out));
                // controlling animation
                vf.getInAnimation().setAnimationListener(mAnimationListener);
                vf.setAutoStart(true);
                vf.setFlipInterval(2000);
                vf.startFlipping();
            }
            else
            {
                Toast.makeText(getActivity(), "cannot download data from server !!!!" + result.toString(), Toast.LENGTH_LONG).show();
            }
            super.onPostExecute(result);
        }
    }

}
