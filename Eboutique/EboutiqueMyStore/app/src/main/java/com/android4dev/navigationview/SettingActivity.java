package com.android4dev.navigationview;
import android.os.Bundle;
import android.preference.PreferenceFragment;

/****        depricated ***************
 public class SettingActivity extends PreferenceActivity {

@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
addPreferencesFromResource(R.xml.preferences);
}
}
 ***/

public class SettingActivity extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
