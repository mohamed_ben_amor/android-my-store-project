package com.android4dev.navigationview;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android4dev.navigationview.com.android4dev.navigationview.adapter.PagerAdapter;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;


    public static String ipAddress;
    public static String port;
    public static String url;
    public static String urlIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        ipAddress = preferences.getString("ip_address","192.168.100.3");
        port = preferences.getString("port","8888");



        //url= "http://"+ipAddress+":"+port+"/all";

        url= "http://"+ipAddress+":"+port+"/allCategorie";

        urlIndex= "http://"+ipAddress+":"+port;

        // Initializing Toolbar and setting it as the actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("A LA UNE"));
        tabLayout.addTab(tabLayout.newTab().setText("TOUS LES RAYON"));
       // tabLayout.addTab(tabLayout.newTab().setText("TOP VENTE"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);


        final ViewPager viewPager = (ViewPager) findViewById(R.id.frame);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {



                    case R.id.draw_idhome:
                        Intent intent = new Intent(MainActivity.this,ContentAccueil.class);
                        startActivity(intent);
                        return true;

                    // For rest of the options we just show a toast on click
                    case R.id.draw_idconnect:
                        Intent intent_connect = new Intent(MainActivity.this,ContentConnecter.class);
                        startActivity(intent_connect);
                        return true;
                    case R.id.draw_idfoundMag:
                        Intent intent_found_mag = new Intent(MainActivity.this,ContentTrouverMagasin.class);
                        startActivity(intent_found_mag);
                        return true;
                    case R.id.draw_idinformation:
                        Intent intent_information = new Intent(MainActivity.this,ContenttInformation.class);
                        startActivity(intent_information);
                        return true;
                    case R.id.draw_idLastArt:
                        Intent intent_lastArt = new Intent(MainActivity.this,ContentHistorique.class);
                        startActivity(intent_lastArt);
                        return true;
                    case R.id.draw_idnousContacter:
                        Intent intent_contactAs = new Intent(MainActivity.this,ContentNousContacter.class);
                        startActivity(intent_contactAs);
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
         actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){


            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);


        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
                case R.id.options:
                   // Intent intent = new Intent(MainActivity.this,ContentAccueil.class);
                    //startActivity(intent);
                   SettingActivity st = new SettingActivity();
                    android.app.FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.origine,st);
                    ft.commit();

                    return true;
                default:
                    break;
            }

        return super.onOptionsItemSelected(item);
    }


}
