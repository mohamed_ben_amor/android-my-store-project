package com.android4dev.navigationview.com.android4dev.navigationview.adapter;

/**
 * Created by Mohamed.BenAmor on 14/12/2015.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android4dev.navigationview.MainActivity;
import com.android4dev.navigationview.R;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Produit;

import java.io.InputStream;
import java.net.URL;
import java.util.List;



public class ListProdModel extends ArrayAdapter<Produit> {

    private List<Produit> produits;
    public ListProdModel(Context context, int resource, List<Produit> prods) {
        super(context, resource,prods);
        this.produits=prods;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.produit_item,parent,false);
        TextView textv = (TextView) rowView.findViewById(R.id.textView);
        final ImageView imgV = (ImageView) rowView.findViewById(R.id.imageViewProd);

        textv.setText(produits.get(position).getDesignation());



        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String imagename = produits.get(position).getPhoto();
                    InputStream is = new URL(MainActivity.urlIndex+"/images/"+imagename).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                    imgV.setImageBitmap(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();



        return rowView;
    }


}
