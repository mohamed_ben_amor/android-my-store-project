package com.android4dev.navigationview.com.android4dev.navigationview.tab1;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

import com.android4dev.navigationview.R;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Publicite;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrateur on 02/01/2016.
 */
public class TabFragmentFlipperPubStatic extends Fragment {

    private Animation.AnimationListener mAnimationListener;
    ViewFlipper vf;

    List<Publicite> listpub = new ArrayList<Publicite>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pub_flipper_static, container, false);
        vf = (ViewFlipper) view.findViewById(R.id.flipperPubStatic);

        vf.setInAnimation(AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.left_in));
        vf.setOutAnimation(AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.left_out));
        // controlling animation
        vf.getInAnimation().setAnimationListener(mAnimationListener);
        vf.setAutoStart(true);
        vf.setFlipInterval(3000);
        vf.startFlipping();


        return view;
    }

}