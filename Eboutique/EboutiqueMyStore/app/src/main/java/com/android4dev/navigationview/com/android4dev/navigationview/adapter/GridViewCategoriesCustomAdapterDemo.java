package com.android4dev.navigationview.com.android4dev.navigationview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android4dev.navigationview.R;
import com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities.Categorie;

import java.util.List;

/**
 * Created by Mohamed.BenAmor on 16/12/2015.
 */
public class GridViewCategoriesCustomAdapterDemo extends ArrayAdapter {
    Context context;

    private List<Categorie> categories;

    public GridViewCategoriesCustomAdapterDemo(Context context, int resource, List<Categorie> cats) {
        super(context, resource,cats);
        this.categories=cats;
    }

   /* public int getCount()
    {
        return 5;
    }*/

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.categorie_produit_grille_row, parent, false);
        final TextView textv = (TextView) rowView.findViewById(R.id.textViewCat);
        final ImageView imgV = (ImageView) rowView.findViewById(R.id.imageViewCat);

        textv.setText(categories.get(position).getNomCategorie());
        //imgV.setImageResource(getImageId(parent.getContext().getApplicationContext(),categories.get(position).getNomPhoto()));
      //  imgV.setImageResource(getImageId(parent.getContext().getApplicationContext(),categories.get(position).getNomPhoto()));
        imgV.setImageResource(getImageId(parent.getContext().getApplicationContext(),categories.get(position).getNomPhoto()));
        //imgV.setImageResource(R.drawable.ordinateur);


        return rowView;


    }

    public static int getImageId(Context context, String imageName) {
        //Toast.makeText(context,"drawable/" + imageName,Toast.LENGTH_LONG).show();
        return context.getResources().getIdentifier(imageName,"drawable", context.getPackageName());
    }

}
