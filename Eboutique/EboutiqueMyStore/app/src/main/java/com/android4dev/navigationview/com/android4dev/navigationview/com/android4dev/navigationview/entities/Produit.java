package com.android4dev.navigationview.com.android4dev.navigationview.com.android4dev.navigationview.entities;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mohamed.BenAmor on 14/12/2015.
 */
public class Produit implements Serializable {

    @SerializedName("idProduit")
    private Long idProduit;

    @SerializedName("designation")
    private String designation;

    @SerializedName("description")
    private String description;

    @SerializedName("prix")
    private double prix;

    @SerializedName("quantite")
    private int quantite;

    @SerializedName("photo")
    private String photo;

    public Produit(Long idProduit, String designation, String description, double prix, int quantite, String photo) {
        this.idProduit = idProduit;
        this.designation = designation;
        this.description = description;
        this.prix = prix;
        this.quantite = quantite;
        this.photo = photo;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public String getDesignation() {
        return designation;
    }

    public String getDescription() {
        return description;
    }

    public double getPrix() {
        return prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public String getPhoto() {
        return photo;
    }
}
