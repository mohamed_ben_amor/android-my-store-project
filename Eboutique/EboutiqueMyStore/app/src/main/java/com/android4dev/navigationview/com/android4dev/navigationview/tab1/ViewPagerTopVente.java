package com.android4dev.navigationview.com.android4dev.navigationview.tab1;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android4dev.navigationview.R;

/**
 * Created by Mohamed.BenAmor on 05/01/2016.
 */
public class ViewPagerTopVente extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.view_pager_top_vente, container, false);
            ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
            viewPager.setAdapter(new ViewAdapter());
            ViewAdapter viewAdapter = new ViewAdapter();
            //viewAdapter.setCount(3);
            viewAdapter.setCount(5);
            viewPager.setAdapter(viewAdapter);
            viewPager.setOffscreenPageLimit(5);
            viewPager.setPageMargin(10);
            viewPager.setClipChildren(false);

        return view;
    }

    private class ViewAdapter extends PagerAdapter {

        private int count = 5;

        public void setCount(int count) {
            this.count = count;
        }

        @Override
        public int getCount() {
            return count;
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TextView textView = new TextView(getActivity());
           // textView.setText(position);
            textView.setBackgroundResource(R.drawable.ic_launcher);
            ((ViewPager)container).addView(textView, 0);
            return textView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((TextView)object);
        }

    }
}
